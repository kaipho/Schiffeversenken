package de.kaipho.schiffeversenken.service;

import de.kaipho.schiffeversenken.model.Game;
import de.kaipho.schiffeversenken.model.Gametype;
import de.kaipho.schiffeversenken.model.Player;

/**
 * Ein Bean um alle laufenden Instanzen des Spiels zu verwalten.
 */
public interface GameService {
    /**
     * Liefert die Instanz des Spieles mit den &uuml;bergebenen Namen.
     *
     * @param name Der Name des gew&uuml;nschten Spiels
     *
     * @return Das Spiel mit dem &uuml;bergebenen Namen
     * @throws RuntimeException Falls kein Spiel mit dem &uuml;bergebenen Namen existiert.
     */
    Game getGame(String name) throws RuntimeException;

    /**
     * Erzeugt ein neues Spiel mit den gew&uuml;nschten Namen und liefert dies zur&uuml;ck.
     *
     * @param name Der gew&uuml;nschte Name des Spiels
     *
     * @return Das erzeugte Spiel
     * @throws RuntimeException Falls breits ein Spiel mit dem &uuml;bergebenen Namen existiert.
     */
    Game createGame(String name) throws RuntimeException;

    /**
     * L&ouml;scht das Spiel mit dem &uuml;bergebenen Namen.
     *
     * @param name Der Name des zu l&ouml;schenden Spiels
     *
     * @throws RuntimeException Falls kein Spiel mit dem &uuml;bergebenen Namen existiert.
     */
    void deleteGame(String name) throws RuntimeException;

    /**
     * L&ouml;scht alle aktuell laufenden Spiele,
     */
    void deleteAllGames();

    /**
     * F&uuml;gt den &uuml;bergebenen Spieler dem Spiel mit dem ausgew&auml;hlten Namen hinzu.
     *
     * @param gameName Das Spiel, zu dem der Spieler hinzugef&uuml;gt werden soll.
     * @param player   Der Spieler, der hinzugef&uuml;gt werden soll.
     *
     * @throws RuntimeException Falls kein Spiel mit dem &uuml;bergebenen Namen existiert, das Spiel bereits voll ist oder bereits ein Spieler mit dem Namen vorhanden ist.
     */
    Game joinGame(String gameName, Player player, Gametype gametyp) throws RuntimeException;

    /**
     * Liefert true, falls ein Spieler mit dem Namen bereits auf dem Server existiert, andernfalls false.
     *
     * @param name Der Name des Spielers.
     */
    boolean isPlayerConnected(String name);
}
