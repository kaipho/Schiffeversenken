package de.kaipho.schiffeversenken.service;

import de.kaipho.schiffeversenken.model.Game;
import de.kaipho.schiffeversenken.model.Gametype;

/**
 * Ein Service der die n&ouml;tigen Funktionen zum erstellen, l&ouml;schen und beitreten von Spielen bereitstellt.
 *
 * @author Tom Schmidt
 * @version 1.0
 */
public interface LoginService {

    /**
     * Erzeugt ein neues Spiel.
     */
    Game createGame(String game, String player, Gametype gametype);

    /**
     * Tritt dem erzeugten Spiel bei.
     */
    Game joinGame(String game, String player, Gametype gametype);

    /**
     * L&ouml;scht das &uuml;bergebene Spiel.
     */
    void deleteGame(String name);
}
