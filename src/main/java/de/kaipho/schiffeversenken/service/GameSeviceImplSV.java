package de.kaipho.schiffeversenken.service;

import de.kaipho.schiffeversenken.exception.GameException;
import de.kaipho.schiffeversenken.model.Game;
import de.kaipho.schiffeversenken.model.Gametype;
import de.kaipho.schiffeversenken.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation des {@link GameService} f�r Schiffe versenken.
 *
 * @author Tom Schmidt
 */

@Service
@Scope("singleton")
public class GameSeviceImplSV implements GameService {
    private Map<String, Game> openGames;
    private List<String> connectedPlayer;

    @Autowired
    ApplicationContext context;

    /**
     * Erzeugt eine neue Instanz von GameService
     */
    public GameSeviceImplSV() {
        openGames = new HashMap<>();
        connectedPlayer = new ArrayList<>();
    }

    @Override
    public Game getGame(String name) throws RuntimeException {
        if (!openGames.containsKey(name)) {
            throw new GameException("Ein Spiel mit den Namen " + name + " existiert nicht.");
        }
        return openGames.get(name);
    }

    @Override
    public Game createGame(String name) throws RuntimeException {
        if (openGames.containsKey(name)) {
            throw new GameException("Ein Spiel mit den Namen " + name + " ist breits vorhanden.");
        }
        Game game = context.getBean(Game.class);
        game.setName(name);
        openGames.put(name, game);
        return game;
    }

    @Override
    public void deleteGame(String name) throws RuntimeException {
        Game game = openGames.get(name);
        if (game == null) {
            throw new GameException("Ein Spiel mit den Namen " + name + " existiert nicht.");
        }
        connectedPlayer.removeAll(game.getBothPlayer());
        openGames.remove(name);
    }

    @Override
    public void deleteAllGames() {
        openGames.clear();
    }

    @Override
    public Game joinGame(String gameName, Player player, Gametype gametyp) throws RuntimeException {
        if (!openGames.containsKey(gameName)) {
            throw new GameException("Ein Spiel mit den Namen " + gameName + " existiert nicht.");
        }
        Game game = openGames.get(gameName);
        if (gametyp == Gametype.TTT) {
            game.addPlayer(player);
        } else {
            game.addPlayerNew(player);
        }
        connectedPlayer.add(player.getName());
        return game;
    }

    @Override
    public boolean isPlayerConnected(String name) {
        return connectedPlayer.contains(name);
    }

}
