package de.kaipho.schiffeversenken.service;

import de.kaipho.schiffeversenken.model.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Implementation des {@link LogicService} f&uuml;r Schiffe versenken.
 *
 * @author Tom Schmidt
 * @version 1.0
 */
@Service
@Scope("session")
public class LogicServiceImplSV implements LogicService {

    @Autowired
    private Game game;

    private boolean finished = false;

    @Override
    public void shoot(String player, int x, int y) throws RuntimeException {

    }

    @Override
    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public boolean isFinished() {
        return finished;
    }

    private void notifyTurn() {
        String playerOnTurn = game.getPlayerOnTurn().getName();
        // new MessageController().sendMessage(game.getEnemy(playerOnTurn).getName() +  " ist am Zug.");
    }

    private void notifyWinner() {
        String playerOnTurn = game.getPlayerOnTurn().getName();
        // new MessageController().sendMessage(playerOnTurn +  " hat gewonnen.");
        // new MessageController().sendMessage(game.getEnemy(playerOnTurn).getName() +  " hat verloren.");
    }
}
