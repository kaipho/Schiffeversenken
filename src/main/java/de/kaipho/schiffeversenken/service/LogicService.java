package de.kaipho.schiffeversenken.service;

import de.kaipho.schiffeversenken.model.Game;
import de.kaipho.schiffeversenken.model.Player;

/**
 * Ein Bean um die Logik des Spiels bereitzustellen.
 *
 * @author Tom Schmidt
 * @version 1.0
 */
public interface LogicService {


    /**
     * F&uuml;hrt einen Schuss auf das Spielfeld von anderen {@link Player} aus
     *
     * @param player Der Spieler der schie&szlig;en will.
     *
     * @throws RuntimeException Falls nicht geschossen werden konnte.
     */
    void shoot(String player, int x, int y) throws RuntimeException;

    /**
     * F&uuml;gt dem Logic Service das zu verwaltende Spiel hinzu.
     *
     * @param game
     */
    void setGame(Game game);

    /**
     * @return true falls das Spiel vorbei ist, andernfalls false.
     */
    boolean isFinished();
}
