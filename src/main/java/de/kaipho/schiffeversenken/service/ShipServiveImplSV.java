package de.kaipho.schiffeversenken.service;

import de.kaipho.schiffeversenken.exception.GameException;
import de.kaipho.schiffeversenken.model.FieldSV;
import de.kaipho.schiffeversenken.model.Ship;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
@Service
@Scope("session")
public class ShipServiveImplSV extends ShipServive {

    FieldSV field;
    List<Integer> ships = new ArrayList<>();

    int shipsToSet = 10;

    @Override
    public void setShip(int size, int startPos, boolean horizontal) throws RuntimeException {
        List<Integer> pos = new ArrayList<>();
        if (field == null) {
            throw new GameException("Kein Spielfeld bekannt");
        }
        if (horizontal) {
            if (!(((startPos / 10) + size) < 11)) {
                throw new GameException("Schiff muss komplett im Spielfeld liegen");
            }
            for (int i = 0; i < size; i++) {
                pos.add(startPos + i * 10);
            }
        } else {
            if (!(((startPos % 10) + size) < 11)) {
                throw new GameException("Schiff muss komplett im Spielfeld liegen");
            }
            for (int i = 0; i < size; i++) {
                pos.add(startPos + i);
            }
        }
        for (Integer integer : ships) {
            if (pos.contains(integer)) {
                throw new GameException("Schiff darf kein anderes Schiff kreuzen");
            }
        }
        for (Ship ship : field.getShips()) {
            if (ship.getParts() == null && ship.getSize() == size) {
                ship.setParts(pos);
                ships.addAll(pos);
                shipsToSet--;
                if (shipsToSet == 0) {
                    field.setIsReady(true);
                }
                return;
            }
        }
        throw new GameException("Schiff konnte nicht gesetzt werden.");
    }

    @Override
    public List<Ship> getShips() {
        return field.getShips();
    }

    @Override
    public boolean isShipOnThisPos(int pos) {
        return false;
    }

    @Override
    public void setField(FieldSV field) {
        this.field = field;
    }

    @Override
    public List<Integer> getShipPos() {
        return ships;
    }

    @Override
    public boolean isGameReady() {
        return shipsToSet == 0;
    }
}
