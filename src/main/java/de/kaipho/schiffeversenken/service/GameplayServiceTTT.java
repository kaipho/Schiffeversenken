package de.kaipho.schiffeversenken.service;

import de.kaipho.schiffeversenken.exception.GameException;
import de.kaipho.schiffeversenken.model.FieldTTT;
import de.kaipho.schiffeversenken.model.Game;
import de.kaipho.schiffeversenken.model.Player;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
@Service
@Scope("session")
public class GameplayServiceTTT implements GameplayService {

    Game game;

    public GameplayServiceTTT() {
    }

    private FieldTTT getField() {
        return ((FieldTTT) game.getPlayerOnTurn().getField());
    }

    public void set(int pos, String playername) {
        if (!isPlayerOnTurn(playername)) {
            throw new GameException("Sie sind nicht am Zug.");
        }
        if (!getField().getShootable()) {
            throw new GameException("Spiel ist vorbei!");
        }
        getField().mark(pos, game.getBothPlayer().get(0).equals(playername) ? true : false);
        game.setPlayerOnTurn(game.getEnemy(playername));
        if (getField().isWon()) {
            Boolean winner = getField().winner();
            if (winner == null) {
                Player even = new Player();
                even.setName("even");
                game.setWinner(even);
            } else if (winner == true) {
                game.setWinner(game.getPlayer(game.getBothPlayer().get(0)));
            } else {
                game.setWinner(game.getPlayer(game.getBothPlayer().get(1)));
            }
        }
    }

    public String[] getFieldImages(String playername) {
        String[] pictures = new String[9];
        Boolean[] marks = ((FieldTTT) game.getPlayer(playername).getField()).getField();
        for (int i = 0; i < marks.length; i++) {
            if (marks[i] == null)
                pictures[i] = "/images/leer.png";
            else if (marks[i] == true)
                pictures[i] = "/images/kreis.png";
            else {
                pictures[i] = "/images/kreuz.png";
            }
        }
        return pictures;
    }

    public void newGame() {
        if (game.getWinner() == null) {
            throw new GameException("Es kann erst ein neues Spiel gestartet werden wenn das alte vorbei ist!");
        }
        game.setWinner(null);
        getField().resetField();
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
        game.setPlayerOnTurn(game.getPlayer(game.getBothPlayer().get(0)));
    }

    public boolean isPlayerOnTurn(String name) {
        return name.equals(game.getPlayerOnTurn().getName());
    }
}
