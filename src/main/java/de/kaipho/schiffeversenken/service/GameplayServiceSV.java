package de.kaipho.schiffeversenken.service;

import de.kaipho.schiffeversenken.exception.GameException;
import de.kaipho.schiffeversenken.model.FieldSV;
import de.kaipho.schiffeversenken.model.Game;
import de.kaipho.schiffeversenken.model.Ship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
@Service(value = "gameplayServiceSV")
@Scope("session")
public class GameplayServiceSV implements GameplayService {

    @Autowired
    Game game;

    private boolean finished = false;

    public boolean isFinished() {
        return finished;
    }

    @Autowired
    ShipServive shipServive;


    public GameplayServiceSV() {
    }

    public FieldSV getField(String playername) {
        return ((FieldSV) game.getPlayer(playername).getField());
    }

    public FieldSV getEnemyField(String playername) {
        if (game.getEnemy(playername) == null) {
            return null;
        }
        return ((FieldSV) game.getEnemy(playername).getField());
    }

    public boolean allShipsSet(String playername) {
        return getField(playername).isReady() && getEnemyField(playername).isReady();
    }

    public boolean isReady(String playername) {
        if (getEnemyField(playername) == null) {
            return false;
        }
        return getEnemyField(playername).getShootable();
    }

    private final String CLOUD = "/images/leer.png";
    private final String WATER = "/images/water.png";
    private final String SHIP_DESTROYED = "/images/kreuz.png";
    private final String SHIP = "/images/kreis.png";

    public void set(int x, int y, String playername) {
        if (!game.isFull()) {
            throw new GameException("Es werden 2 Spieler zum Spielen ben\u00f6tigt!");
        }
        Ship ship = getEnemyField(playername).mark(x, y);
        getField(playername).setShootable(true);

        game.setPlayerOnTurn(game.getEnemy(playername));
        if (getEnemyField(playername).isWon()) {
            game.setWinner(game.getPlayer(playername));
            finished = true;
        }
    }

    public String[][] getFieldImages(String playername) {
        List<Integer> ships = shipServive.getShipPos();

        String[][] pictures = new String[10][10];
        Boolean[][] field = getField(playername).getField();
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == Boolean.TRUE) {
                    pictures[j][i] = WATER;
                } else if (ships.contains(i * 10 + j)) {
                    pictures[j][i] = SHIP;
                } else {
                    pictures[j][i] = CLOUD;
                }
                if (field[i][j] == Boolean.FALSE) {
                    pictures[j][i] = SHIP_DESTROYED;
                }
            }

        }
        return pictures;
    }

    public String[][] getEnemyFieldImages(String playername) {
        if (game.getWinner() != null) {
            List<Integer> ships = new ArrayList<>();
            for (Ship ship : getEnemyField(playername).getShips()) {
                ships.addAll(ship.getParts());
            }
            String[][] pictures = new String[10][10];
            Boolean[][] field = getEnemyField(playername).getField();
            for (int i = 0; i < field.length; i++) {
                for (int j = 0; j < field[i].length; j++) {
                    if (field[i][j] == Boolean.TRUE) {
                        pictures[j][i] = WATER;
                    } else if (ships.contains(i * 10 + j)) {
                        pictures[j][i] = SHIP;
                    } else {
                        pictures[j][i] = CLOUD;
                    }
                    if (field[i][j] == Boolean.FALSE) {
                        pictures[j][i] = SHIP_DESTROYED;
                    }
                }

            }
            return pictures;
        }
        String[][] pictures = new String[10][10];
        try {
            Boolean[][] field = getEnemyField(playername).getField();
            for (int i = 0; i < field.length; i++) {
                for (int j = 0; j < field[i].length; j++) {
                    if (field[i][j] == Boolean.TRUE) {
                        pictures[j][i] = WATER;
                    } else if (field[i][j] == Boolean.FALSE) {
                        pictures[j][i] = SHIP;
                    } else {
                        pictures[j][i] = CLOUD;
                    }
                }

            }
            return pictures;
        } catch (RuntimeException e) {
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    pictures[j][i] = CLOUD;
                }

            }
            return pictures;
        }
    }

//    public void newGame() {
//        if(game.getWinner() == null) {
//            throw new GameException("Es kann erst ein neues Spiel gestartet werden wenn das alte vorbei ist!");
//        }
//        game.setWinner(null);
//        getField().resetField();
//    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
        game.setPlayerOnTurn(game.getPlayer(game.getBothPlayer().get(0)));
    }

    public boolean isPlayerOnTurn(String name) {
        return getField(name).getShootable();
    }
}
