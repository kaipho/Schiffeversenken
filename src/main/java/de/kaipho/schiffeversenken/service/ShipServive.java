package de.kaipho.schiffeversenken.service;

import de.kaipho.schiffeversenken.model.FieldSV;
import de.kaipho.schiffeversenken.model.Ship;

import java.util.List;

/**
 * Ein Service, der sich um das setzen von Schiffen k&uuml;mmert.
 *
 * @author Tom Schmidt
 * @version 1.0
 */
public abstract class ShipServive {

    /**
     * Versucht ein Schiff auf das Spielfeld zu setzen.
     *
     * @param size       Die L&auml;nge des Schiffes.
     * @param startPos   Die Position vom ersten Teil des Schiffes.
     * @param horizontal Soll das Schiff horizontal (true) oder vertikal (false) stehen.
     *
     * @throws RuntimeException Falls das Schiff nicht plaziert werden konnte.
     */
    public abstract void setShip(int size, int startPos, boolean horizontal) throws RuntimeException;

    /**
     * &Uuml;berpr&uuml;ft ob an der &uuml;bergebenen Position ein Schiffteil liegt.
     *
     * @param pos Die zu pr&uuml;fende Position
     *
     * @return true, falls ein Schiffteil gefunden wurde, sonst false.
     */
    public abstract boolean isShipOnThisPos(int pos);

    /**
     * Setzt ein Feld in den ShipSewrvice
     *
     * @param field Das Spielfeld des Spielers
     */
    public abstract void setField(FieldSV field);

    public abstract List<Integer> getShipPos();

    public abstract boolean isGameReady();

    public abstract List<Ship> getShips();

}
