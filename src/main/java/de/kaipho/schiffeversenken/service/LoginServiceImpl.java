package de.kaipho.schiffeversenken.service;

import de.kaipho.schiffeversenken.exception.GameException;
import de.kaipho.schiffeversenken.model.Game;
import de.kaipho.schiffeversenken.model.Gametype;
import de.kaipho.schiffeversenken.model.Player;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
@Service
@Scope("session")
public class LoginServiceImpl implements LoginService {

    private GameService gameService;
    private Player player;

    public LoginServiceImpl(GameService gameService, Player player) {
        this.gameService = gameService;
        this.player = player;
    }

    @Override
    public Game createGame(String game, String player, Gametype gametype) {
        validateStrings(game, player);
        gameService.createGame(game);
        return joinGame(game, player, gametype);
    }

    @Override
    public Game joinGame(String game, String player, Gametype gametype) {
        validateStrings(game, player);
        this.player.setName(player);
        return gameService.joinGame(game, this.player, gametype);
    }

    @Override
    public void deleteGame(String name) {
        gameService.deleteGame(name);
    }

    public void validateStrings(String game, String player) {
        if ("".equals(player)) {
            throw new GameException("Bitte einen Spielernamen angeben!");
        }
        if ("".equals(game)) {
            throw new GameException("Bitte einen Spielnamen angeben!");
        }
    }
}
