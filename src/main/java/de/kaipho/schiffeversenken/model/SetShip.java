package de.kaipho.schiffeversenken.model;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public class SetShip {
    private int posx;
    private int posy;
    private int size;
    private boolean horizontal;

    public SetShip() {
    }

    public int getPosx() {
        return posx;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    public void setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
    }
}
