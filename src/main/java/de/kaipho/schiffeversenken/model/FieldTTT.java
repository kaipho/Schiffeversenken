package de.kaipho.schiffeversenken.model;

import de.kaipho.schiffeversenken.exception.GameException;

/**
 * Ein Modell f&uuml;r ein 10*10 Spielfeld von SchiffeVersenken
 */
public class FieldTTT implements Field {
    public static final Boolean PLAYER1 = Boolean.TRUE; //
    public static final Boolean PLAYER2 = Boolean.FALSE; // getroffen
    public static final Boolean LEER = null;

    private Boolean[] field;
    private int[][] gewonnen = new int[][]{{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}};

    public FieldTTT() {
        field = new Boolean[]{null, null, null, null, null, null, null, null, null};
    }

    /**
     * Markiert die &uuml;bergebenen Koordinaten entweder als Wasser oder als gertoffenes Ship.
     *
     * return Das Schiff, falls eins getroffen wurde.
     *
     * @throws RuntimeException Falls bereits auf das Feld geschossen wurde.
     */
    public void mark(int x, boolean player) throws RuntimeException {
        if (field[x] != null) {
            throw new GameException("Auf dieses Feld wurde bereits gesetzt.");
        }
        field[x] = player;
    }

    public Boolean winner() {
        for (int i = 0; i < gewonnen.length; i++) {
            if (field[gewonnen[i][0]] == field[gewonnen[i][1]] && field[gewonnen[i][1]] == field[gewonnen[i][2]] && field[gewonnen[i][0]] != null)
                return field[gewonnen[i][0]];
        }
        for (int i = 0; i < field.length; i++) {
            if (field[i] == null)
                throw new RuntimeException("Darf noch nicht aufgerufen werden!");
        }
        return null; // Unentschieden
    }

    /**
     * @return true falls alle Schiffe versenkt wurden, andernfalls false.
     */
    public boolean isWon() {
        for (int i = 0; i < gewonnen.length; i++) {
            if (field[gewonnen[i][0]] == field[gewonnen[i][1]] && field[gewonnen[i][1]] == field[gewonnen[i][2]] && field[gewonnen[i][0]] != null)
                return true;
        }
        for (int i = 0; i < field.length; i++) {
            if (field[i] == null)
                return false;
        }
        return true;
    }

    public void resetField() {
        field = new Boolean[]{null, null, null, null, null, null, null, null, null};
    }

    public boolean getShootable() {
        return !isWon();
    }

    @Override
    public void setShootable(boolean shootable) {
        // Nicht erlaubt
    }

    public Boolean[] getField() {
        return field;
    }
}
