package de.kaipho.schiffeversenken.model;

import de.kaipho.schiffeversenken.exception.GameException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Ein Modell f&uuml;r ein 10*10 Spielfeld von SchiffeVersenken
 */
@Component(value = "PlayerField")
@Scope("session")
public class FieldSV implements Field {
    public static final Boolean KREIS = Boolean.TRUE; // daneben
    public static final Boolean KREUZ = Boolean.FALSE; // getroffen
    public static final Boolean LEER = null;

    private Boolean[][] field;
    private List<Ship> ships;
    private boolean isShootable;
    private boolean isReady;

    public boolean isReady() {
        return isReady;
    }

    public void setIsReady(boolean isReady) {
        this.isReady = isReady;
    }

    private int shipParts;

    @Autowired
    private ApplicationEventPublisher publisher;

    public FieldSV() {
        field = new Boolean[10][10];

        // TODO Schiife einmal &uuml;ber Spring generieren


        ships = new ArrayList<>(Arrays.asList(new Ship[]{new Ship(2), new Ship(2), new Ship(2), new Ship(2), new Ship(3), new Ship(3), new Ship(3), new Ship(4), new Ship(4), new Ship(5)}));
        shipParts = 30;
        isShootable = false;
    }

    public void setShootable(boolean shootable) {
        this.isShootable = shootable;
        // TODO KI
/*        if (isWon()) {
            return;
        }
        if (shootable) {
            publisher.publishEvent(new TurnChangeEvent(this));
            this.isShootable = false;
        }*/
    }

    public boolean getShootable() {
        return isShootable;
    }

    /**
     * Markiert die &uuml;bergebenen Koordinaten entweder als Wasser oder als gertoffenes Ship.
     *
     * return Das Schiff, falls eins getroffen wurde.
     *
     * @throws RuntimeException Falls bereits auf das Feld geschossen wurde.
     */
    public Ship mark(int x, int y) throws RuntimeException {
        if (!isShootable) {
            throw new GameException("Sie sind nicht am Zug!");
        }
        if (field[x][y] == LEER) {
            for (Ship ship : ships) {
                if (ship.getParts().contains(x * 10 + y)) {
                    field[x][y] = KREUZ;
                    shipParts--;
                    isShootable = false;
                    ship.destroyPart();
                    return ship;
                }
                field[x][y] = KREIS;
                isShootable = false;
            }
            return null;
        } else {
            throw new GameException("Auf dieses Feld wurde Bereits geschossen!");
        }
    }

    //TODO Setzte Schiffe Methode


    public List<Ship> getShips() {
        return ships;
    }

    public void setShips(List<Ship> ships) {
        this.ships = ships;
    }

    /**
     * @return true falls alle Schiffe versenkt wurden, andernfalls false.
     */
    public boolean isWon() {
        return (shipParts == 0);
    }

    public Boolean[][] getField() {
        return field;
    }
}
