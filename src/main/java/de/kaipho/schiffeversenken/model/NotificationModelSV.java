package de.kaipho.schiffeversenken.model;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public class NotificationModelSV {

    String[][] fieldPlayer;
    String[][] fieldEnemy;

    int count2E;
    int count3E;
    int count4E;
    int count5E;

    int count2P;
    int count3P;
    int count4P;
    int count5P;

    String username;
    boolean onTurn;
    String winner;

    public NotificationModelSV(String[][] fieldPlayer, String[][] fieldEnemy, int count2E, int count3E, int count4E, int count5E, int count2P, int count3P, int count4P, int count5P, String username, boolean onTurn, String winner) {
        this.fieldPlayer = fieldPlayer;
        this.fieldEnemy = fieldEnemy;
        this.count2E = count2E;
        this.count3E = count3E;
        this.count4E = count4E;
        this.count5E = count5E;
        this.count2P = count2P;
        this.count3P = count3P;
        this.count4P = count4P;
        this.count5P = count5P;
        this.username = username;
        this.onTurn = onTurn;
        this.winner = winner;
    }

    public String[][] getFieldPlayer() {
        return fieldPlayer;
    }

    public void setFieldPlayer(String[][] fieldPlayer) {
        this.fieldPlayer = fieldPlayer;
    }

    public String[][] getFieldEnemy() {
        return fieldEnemy;
    }

    public void setFieldEnemy(String[][] fieldEnemy) {
        this.fieldEnemy = fieldEnemy;
    }

    public int getCount2E() {
        return count2E;
    }

    public void setCount2E(int count2E) {
        this.count2E = count2E;
    }

    public int getCount3E() {
        return count3E;
    }

    public void setCount3E(int count3E) {
        this.count3E = count3E;
    }

    public int getCount4E() {
        return count4E;
    }

    public void setCount4E(int count4E) {
        this.count4E = count4E;
    }

    public int getCount5E() {
        return count5E;
    }

    public void setCount5E(int count5E) {
        this.count5E = count5E;
    }

    public int getCount2P() {
        return count2P;
    }

    public void setCount2P(int count2P) {
        this.count2P = count2P;
    }

    public int getCount3P() {
        return count3P;
    }

    public void setCount3P(int count3P) {
        this.count3P = count3P;
    }

    public int getCount4P() {
        return count4P;
    }

    public void setCount4P(int count4P) {
        this.count4P = count4P;
    }

    public int getCount5P() {
        return count5P;
    }

    public void setCount5P(int count5P) {
        this.count5P = count5P;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isOnTurn() {
        return onTurn;
    }

    public void setOnTurn(boolean onTurn) {
        this.onTurn = onTurn;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }
}
