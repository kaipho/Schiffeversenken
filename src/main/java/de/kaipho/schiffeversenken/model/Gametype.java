package de.kaipho.schiffeversenken.model;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public enum Gametype {
    TTT,
    SV
}
