package de.kaipho.schiffeversenken.model;

import java.util.List;

/**
 * Ein Modell f&uuml;r ein Ship von Schiffe versenken.
 *
 * @author Tom Schmidt
 * @version 1.0
 */
public class Ship {

    private List<Integer> parts;
    private int size;
    private int destroyedParts;

    /**
     * Erstellt ein neues Ship mit der &uuml;bergebenen Gr&ouml;&szlig;e
     *
     * @param size Die gr&ouml;&szlig;e des Schiffes
     */
    public Ship(int size) {
        this.size = size;
        this.destroyedParts = 0;
    }

    /**
     * Setzt die Teile des Schiffes
     *
     * @param parts Eine Liste mit den Positionen der Teile
     *
     * @throws RuntimeException Falls zu wenig oder zu viele Teile &uuml;bergeben wurden.
     */
    public void setParts(List<Integer> parts) throws RuntimeException {
        if (parts.size() != size)
            throw new RuntimeException("Die gr\u00f6\u00dfe des Schiffes stimmt nicht mit der Anzahl der Teile \u00fcberein.");
        this.parts = parts;
    }

    public void destroyPart() {
        destroyedParts++;
    }

    public List<Integer> getParts() {
        return parts;
    }

    /**
     * @return Die L&auml;nge des Schiffes.
     */
    public int getSize() {
        return size;
    }

    /**
     * @return true, falls alle Teile des Schiffes getroffen wurden.
     */
    public boolean isDestroyed() {
        return size == destroyedParts;
    }
}
