package de.kaipho.schiffeversenken.model;

import de.kaipho.schiffeversenken.eventtest.TurnChangeEvent;
import de.kaipho.schiffeversenken.eventtest.model.FieldSVKI;
import de.kaipho.schiffeversenken.exception.GameException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Modell f&uuml;r ein Spiel zwischen 2 Spielern
 */
@Component
@Scope("session")
public class Game {
    private String name;
    private Player player1;
    private Player player2;
    private Player playerOnTurn;
    private Player winner;

    @Autowired
    public Game(Player ai, FieldSVKI field2) {
        // TODO KI
/*        player2 = ai;
        ai.setName("AI");
        ai.setField(field2);*/
    }

    /**
     * Initialisiert ein neues PvP Spiel.
     *
     * @param name Der Name des Spiels.
     */
    public Game(String name) {
        this(name, false);
    }

    /**
     * Initialisiert ein neues Spiel.
     *
     * @param name  Der Name des Spiels.
     * @param vsBot Soll der gegner ein Bot sein?
     */
    public Game(String name, boolean vsBot) {
        this.name = name;
    }

    public void addPlayer(Player player) {
        if (isFull()) {
            throw new GameException("Das Spiel " + name + " hat bereits die maximale Zahl an Spielern erreicht.");
        } else if (player1 == null) {
            player1 = player;
        } else if (player1.getName().equals(player.getName())) {
            throw new GameException("Es ist bereits ein Spieler mit dem Namen " + player.getName() + " angemeldet.");
        } else {
            player2 = player;
            player.setField(player1.getField());
        }
    }

    public void addPlayerNew(Player player) {
        if (isFull()) {
            throw new GameException("Das Spiel " + name + " hat bereits die maximale Zahl an Spielern erreicht.");
        } else if (player1 == null) {
            player1 = player;
        } else if (player1.getName().equals(player.getName())) {
            throw new GameException("Es ist bereits ein Spieler mit dem Namen " + player.getName() + " angemeldet.");
        } else {
            player2 = player;
        }
    }

    public Player getPlayer(String name) {
        try {
            if (name.equals(player1.getName())) {
                return player1;
            }
            if (name.equals(player2.getName())) {
                return player2;
            }
        } catch (Exception e) {
        }
        throw new GameException("Spieler wurde im Spiel " + name + " nicht gefunden!");
    }

    public List<String> getBothPlayer() {
        List<String> player = new ArrayList<>();
        player.add(player1.getName());
        if (player2 != null) {
            player.add(player2.getName());
        }
        return player;
    }

    public Player getEnemy(String name) {
        if (!isFull()) {
            return null;
        }
        if (name.equals(player1.getName())) {
            return player2;
        }
        return player1;
    }

    public Player getPlayerOnTurn() {
        return playerOnTurn;
    }

    public Player getPlayerOnTurnSV() {
        if (((FieldSV) player1.getField()).getShootable()) {
            return player1;
        }
        return player2;
    }

    @EventListener
    public void handleTurnChange(TurnChangeEvent event) {
        if(event.getField().equals(player1.getField())) {
            player2.getField().setShootable(true);
        } else {
            player1.getField().setShootable(true);
        }
    }

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFull() {
        return player1 != null && player2 != null;
    }

    public void setPlayerOnTurn(Player playerOnTurn) {
        this.playerOnTurn = playerOnTurn;
    }
}
