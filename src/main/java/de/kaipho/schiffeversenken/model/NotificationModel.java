package de.kaipho.schiffeversenken.model;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public class NotificationModel {

    String[] field;
    String username;
    boolean onTurn;
    String winner;

    public NotificationModel(String[] field, String username, boolean onTurn, String winner) {
        this.field = field;
        this.username = username;
        this.onTurn = onTurn;
        this.winner = winner;
    }

    public String[] getField() {
        return field;
    }

    public void setField(String[] field) {
        this.field = field;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isOnTurn() {
        return onTurn;
    }

    public void setOnTurn(boolean onTurn) {
        this.onTurn = onTurn;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }
}
