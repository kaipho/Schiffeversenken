package de.kaipho.schiffeversenken.model;

/**
 * Modell f&uuml;r ein Spieler
 */

public class Player {
    private String name;

    Field field;

    public Player() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }
}