package de.kaipho.schiffeversenken.model;

/**
 * Model um die Login Daten zu speichern.
 *
 * @author Tom Schmidt
 * @version 1.0
 */

public class SessionInfo {
    private String user;
    private String game;

    public SessionInfo() {
    }

    public SessionInfo(String game, String user) {
        this.game = game;
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    @Override
    public String toString() {
        return "SessionInfo{" +
                "user='" + user + '\'' +
                ", game='" + game + '\'' +
                '}';
    }
}
