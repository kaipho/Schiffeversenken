package de.kaipho.schiffeversenken.model;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public interface Field {

    boolean getShootable();

    void setShootable(boolean shootable);

    boolean isWon();
}
