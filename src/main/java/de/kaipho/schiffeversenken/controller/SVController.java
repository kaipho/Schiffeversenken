package de.kaipho.schiffeversenken.controller;

import de.kaipho.schiffeversenken.exception.GameException;
import de.kaipho.schiffeversenken.model.*;
import de.kaipho.schiffeversenken.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Ein Controller um den Spielfluss während TIC TAC TOE zu steuern.
 *
 * @author Tom Schmidt
 * @version 1.0
 */
@Controller
@Scope("session")
@SessionAttributes(value = {"player"})
@RequestMapping("/sv")
public class SVController {

    @Autowired
    @Qualifier(value = "svLoginService")
    private LoginService loginService;
    @Autowired
    private GameplayServiceSV gameplayService;
    @Autowired
    private GameService gameService;
    @Autowired
    private LogicService logicService;
    @Autowired
    private ShipServive shipService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String showLoginPage(HttpSession session) {
        session.invalidate();
        return "creategame";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    String createNewGame(@RequestBody SessionInfo sessionInfo, ModelMap map) {
        map.put("player", sessionInfo.getUser());
        gameplayService.setGame(loginService.createGame(sessionInfo.getGame(), sessionInfo.getUser(), Gametype.SV));
        return "OK"; // Damit die Error funktion im js weiß, dass doch alles OK war. Komischer Fehler....
    }

    @RequestMapping(value = "/join", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    String joinExistingGame(@RequestBody SessionInfo sessionInfo, ModelMap map) {
        map.put("player", sessionInfo.getUser());
        gameplayService.setGame(loginService.joinGame(sessionInfo.getGame(), sessionInfo.getUser(), Gametype.SV));
        return "OK"; // Damit die Error funktion im js weiß, dass doch alles OK war. Komischer Fehler....
    }

    @RequestMapping(value = "/play", method = RequestMethod.GET)
    public String showPlayTTT(ModelMap map) {
        if (shipService.isGameReady()) {
            map.put("field", createShootableField());
            map.put("fielde", createEnemyField());
            map.put("count2", getShipCount(2));
            map.put("count3", getShipCount(3));
            map.put("count4", getShipCount(4));
            map.put("count5", getShipCount(5));
            return "playsv";
        } else {
            shipService.setField((FieldSV) gameplayService.getGame().getPlayer((String) map.get("player")).getField());
            map.put("field", createField());
            map.put("count2", getShipCount(2));
            map.put("count3", getShipCount(3));
            map.put("count4", getShipCount(4));
            map.put("count5", getShipCount(5));
            return "setshipssv";
        }
    }

    private String DIV_ROW = "<div class='row' style='margin-left: -15px; margin-right: -15px;'>";
    private String DIV_HEAD_BEGIN = "<div class='col-xs-1 center'>";
    private String DIV_HEAD_END = "</div>";
    private String DIV_BODY_BEGIN = "<div class='col-xs-1'><a href='#' id='field-";
    private String DIV_BODYE_BEGIN = "<div class='col-xs-1'><a href='' id='fieldE-";
    private String DIV_BODY_MIDDLE = "' class='thumbnail' onmouseover='showShipPos(this)' onmouseout='endShowShipPos(this)' onclick='setShip(this); return false;'> <img src='";
    private String DIV_BODY_MIDDLE_SHOOT = "' class='thumbnail' onclick='shoot(this); return false;'> <img src='";
    private String DIV_BODY_MIDDLE_EMPTY = "' class='thumbnail' > <img src='";
    private String DIV_BODY_END = "' /> </a></div>";
    private String[] LETTERS = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};

    private String SHIP = "/images/kreuz.png";
    private String WATER = "/images/water.png";

    private String createField() {
        StringBuilder builder = new StringBuilder();
        builder.append(DIV_ROW);
        builder.append(DIV_HEAD_BEGIN);
        builder.append(DIV_HEAD_END);

        // HEAD
        for (int i = 1; i <= 10; i++) {
            builder.append(DIV_HEAD_BEGIN);
            builder.append(i);
            builder.append(DIV_HEAD_END);
        }
        builder.append(DIV_HEAD_END);

        // BODY
        for (int i = 0; i <= 9; i++) {
            builder.append(DIV_ROW);
            builder.append(DIV_HEAD_BEGIN);
            builder.append(LETTERS[i]);
            builder.append(DIV_HEAD_END);
            for (int j = 0; j <= 9; j++) {
                builder.append(DIV_BODY_BEGIN);
                builder.append(i);
                builder.append("-");
                builder.append(j);
                builder.append(DIV_BODY_MIDDLE);
                builder.append(getPicture(j, i));
                builder.append(DIV_BODY_END);
            }
            builder.append(DIV_HEAD_END);
        }
        return builder.toString();
    }

    private String createShootableField() {
        StringBuilder builder = new StringBuilder();
        builder.append(DIV_ROW);
        builder.append(DIV_HEAD_BEGIN);
        builder.append(DIV_HEAD_END);

        // HEAD
        for (int i = 1; i <= 10; i++) {
            builder.append(DIV_HEAD_BEGIN);
            builder.append(i);
            builder.append(DIV_HEAD_END);
        }
        builder.append(DIV_HEAD_END);

        // BODY
        for (int i = 0; i <= 9; i++) {
            builder.append(DIV_ROW);
            builder.append(DIV_HEAD_BEGIN);
            builder.append(LETTERS[i]);
            builder.append(DIV_HEAD_END);
            for (int j = 0; j <= 9; j++) {
                builder.append(DIV_BODY_BEGIN);
                builder.append(i);
                builder.append("-");
                builder.append(j);
                builder.append(DIV_BODY_MIDDLE_SHOOT);
                builder.append(getPicture(j, i));
                builder.append(DIV_BODY_END);
            }
            builder.append(DIV_HEAD_END);
        }
        return builder.toString();
    }

    private String createEnemyField() {
        StringBuilder builder = new StringBuilder();
        builder.append(DIV_ROW);
        builder.append(DIV_HEAD_BEGIN);
        builder.append(DIV_HEAD_END);

        // HEAD
        for (int i = 1; i <= 10; i++) {
            builder.append(DIV_HEAD_BEGIN);
            builder.append(i);
            builder.append(DIV_HEAD_END);
        }
        builder.append(DIV_HEAD_END);

        // BODY
        for (int i = 0; i <= 9; i++) {
            builder.append(DIV_ROW);
            builder.append(DIV_HEAD_BEGIN);
            builder.append(LETTERS[i]);
            builder.append(DIV_HEAD_END);
            for (int j = 0; j <= 9; j++) {
                builder.append(DIV_BODYE_BEGIN);
                builder.append(i);
                builder.append("-");
                builder.append(j);
                builder.append(DIV_BODY_MIDDLE_EMPTY);
                builder.append(getPicture(j, i));
                builder.append(DIV_BODY_END);
            }
            builder.append(DIV_HEAD_END);
        }
        return builder.toString();
    }

    private String getPicture(int x, int y) {
        List<Integer> ships = shipService.getShipPos();
        if (ships.contains(x * 10 + y)) {
            return SHIP;
        } else {
            return WATER;
        }
    }

    private int getShipCount(int size) {
        int count = 0;
        for (Ship ship : shipService.getShips()) {
            if (ship.getSize() == size && ship.getParts() == null) {
                count++;
            }
        }
        return count;
    }

    private int getRemainShipCount(int size) {
        int count = 0;
        for (Ship ship : shipService.getShips()) {
            if (ship.getSize() == size && !ship.isDestroyed()) {
                count++;
            }
        }
        return count;
    }

    private int getRemainEnemyShipCount(int size, ModelMap map) {
        int count = 0;
        if (gameplayService.getEnemyField((String) map.get("player")) == null) {
            return 0;
        }
        for (Ship ship : gameplayService.getEnemyField((String) map.get("player")).getShips()) {
            if (ship.getSize() == size && !ship.isDestroyed()) {
                count++;
            }
        }
        return count;
    }

    @RequestMapping(value = "/play/set", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    String set(@RequestBody SetShip ship, ModelMap map) {
        shipService.setShip(ship.getSize(), ship.getPosx() * 10 + ship.getPosy(), ship.isHorizontal());
        if (shipService.isGameReady()) {
            if (gameplayService.getEnemyField((String) map.get("player")) != null && gameplayService.getEnemyField((String) map.get("player")).getShootable() == true) {
                return "RELOAD";
            } else {
                gameplayService.getField((String) map.get("player")).setShootable(true);
                return "RELOAD";
            }
        }
        return "OK";
    }

    @RequestMapping(value = "/play/shoot", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    String shoot(@RequestBody SetShip ship, ModelMap map) {
        if (gameplayService.getGame().getWinner() != null) {
            throw new GameException("Spiel ist vorbei!");
        }
        gameplayService.set(ship.getPosx(), ship.getPosy(), (String) map.get("player"));
        return "NOTHING";
    }

    @RequestMapping(value = "/play/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    NotificationModelSV update(ModelMap map) {
        if(map.get("player") == null) {
            return null;
        }
        String winner = gameplayService.getGame().getWinner() != null ? gameplayService.getGame().getWinner().getName() : null;
        String player = (String) map.get("player");
        String playerText = gameplayService.allShipsSet((String) map.get("player")) ? player : "Warten auf zweiten Spieler...";
        return new NotificationModelSV(gameplayService.getEnemyFieldImages((String) map.get("player")), gameplayService.getFieldImages((String) map.get("player")),
                getRemainShipCount(2), getRemainShipCount(3), getRemainShipCount(4), getRemainShipCount(5),
                getRemainEnemyShipCount(2, map), getRemainEnemyShipCount(3, map), getRemainEnemyShipCount(4, map), getRemainEnemyShipCount(5, map), playerText, !gameplayService.isPlayerOnTurn((String) map.get("player")), winner);
    }

    @RequestMapping(value = "/play/newgame", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    String newGame(ModelMap map) {
//        gameplayService.newGame();
        return "OK";
    }

    @ExceptionHandler(GameException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleGameException(GameException ex, HttpServletResponse response) {
        ex.printStackTrace();
        return ex.getMessage();
    }
}
