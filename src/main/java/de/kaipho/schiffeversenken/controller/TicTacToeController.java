package de.kaipho.schiffeversenken.controller;

import de.kaipho.schiffeversenken.exception.GameException;
import de.kaipho.schiffeversenken.model.Gametype;
import de.kaipho.schiffeversenken.model.NotificationModel;
import de.kaipho.schiffeversenken.model.SessionInfo;
import de.kaipho.schiffeversenken.service.GameplayServiceTTT;
import de.kaipho.schiffeversenken.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Ein Controller um den Spielfluss während TIC TAC TOE zu steuern.
 *
 * @author Tom Schmidt
 * @version 1.0
 */
@Controller
@Scope("session")
@SessionAttributes(value = {"player"})
@RequestMapping("/ttt")
public class TicTacToeController {

    @Autowired
    @Qualifier(value = "tttLoginService")
    LoginService loginService;
    @Autowired
    GameplayServiceTTT gameplayService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String showLoginPage(HttpSession session) {
        session.invalidate();
        return "creategame";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    String createNewGame(@RequestBody SessionInfo sessionInfo, ModelMap map) {
        map.put("player", sessionInfo.getUser());
        gameplayService.setGame(loginService.createGame(sessionInfo.getGame(), sessionInfo.getUser(), Gametype.TTT));
        return "OK"; // Damit die Error funktion im js weiß, dass doch alles OK war. Komischer Fehler....
    }

    @RequestMapping(value = "/join", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    String joinExistingGame(@RequestBody SessionInfo sessionInfo, ModelMap map) {
        map.put("player", sessionInfo.getUser());
        gameplayService.setGame(loginService.joinGame(sessionInfo.getGame(), sessionInfo.getUser(), Gametype.TTT));
        return "OK"; // Damit die Error funktion im js weiß, dass doch alles OK war. Komischer Fehler....
    }

    @RequestMapping(value = "/play", method = RequestMethod.GET)
    public String showPlayTTT(ModelMap map) {
        return "playttt";
    }

    @RequestMapping(value = "/play/set", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    String set(@RequestBody String field, ModelMap map) {
        gameplayService.set(Integer.parseInt(field.split(":")[1].substring(1, 2)), (String) map.get("player"));
        return "OK";
    }

    @RequestMapping(value = "/play/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    NotificationModel update(ModelMap map) {
        String winner = gameplayService.getGame().getWinner() != null ? gameplayService.getGame().getWinner().getName() : null;
        String player = (String) map.get("player");
        String playerText = gameplayService.getGame().isFull() ? player : "Warten auf zweiten Spieler.";
        return new NotificationModel(gameplayService.getFieldImages(player), playerText, gameplayService.isPlayerOnTurn(player), winner);
    }

    @RequestMapping(value = "/play/newgame", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    String newGame(ModelMap map) {
        gameplayService.newGame();
        return "OK";
    }

    @ExceptionHandler(GameException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleGameException(GameException ex, HttpServletResponse response) {
        return ex.getMessage();
    }
}
