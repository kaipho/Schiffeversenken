package de.kaipho.schiffeversenken.statetest;

import de.kaipho.schiffeversenken.service.GameService;
import de.kaipho.schiffeversenken.statetest.statemachine.Events;
import de.kaipho.schiffeversenken.statetest.statemachine.States;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
@Service
public class GameplayServiceBattleShip {

    private final StateMachine<States, Events> stateMachine;
    private final GameService gameService;


    @Autowired
    public GameplayServiceBattleShip(StateMachine<States, Events> stateMachine, GameService gameService) {
        this.stateMachine = stateMachine;
        this.gameService = gameService;
    }


}
