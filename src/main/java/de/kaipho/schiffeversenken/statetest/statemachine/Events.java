package de.kaipho.schiffeversenken.statetest.statemachine;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public enum Events {
    PLAYER_MAKEING_TURN, AI_MAKEING_TURN;
}
