package de.kaipho.schiffeversenken.statetest.statemachine;

import de.kaipho.schiffeversenken.eventtest.model.FieldSVKI;
import de.kaipho.schiffeversenken.model.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
@WithStateMachine
public class TransitionActions {

    @Autowired
    ApplicationContext context;

    @OnTransition(source = "AI_ON_TURN", target = "PLAYER_ON_TURN")
    public void toPlayer() throws InterruptedException {
        System.err.println("AI macht zug...");
//        handleTurnChange();
    }

    @OnTransition(source = "PLAYER_ON_TURN", target = "AI_ON_TURN")
    public void toAI() throws InterruptedException {
        System.err.println("PLAYER macht zug...");
    }

    public void handleTurnChange() {
        FieldSVKI field = context.getBean(FieldSVKI.class);
        int target = getShootableFields(field).get(0);
        System.out.println("Die KI schießt auf das Feld: " + target);
        FieldSVKI aiField = field;
        aiField.mark(target / 10, target % 10);
    }

    private List<Integer> getShootableFields(Field field) {
        FieldSVKI aiField = (FieldSVKI) field;
        List<Integer> shootableFields = new LinkedList<>();
        for (int i = 0; i < aiField.getField().length; i++) {
            for (int j = 0; j < aiField.getField()[i].length; j++) {
                if (aiField.getField()[i][j] == null) {
                    shootableFields.add(i * 10 + j);
                }
            }
        }
        Collections.shuffle(shootableFields);
        return shootableFields;
    }
}
