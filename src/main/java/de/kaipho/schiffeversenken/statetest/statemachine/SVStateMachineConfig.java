package de.kaipho.schiffeversenken.statetest.statemachine;

import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
@Configuration
@EnableStateMachine
public class SVStateMachineConfig extends EnumStateMachineConfigurerAdapter<States, Events> {

    @Override
    public void configure(StateMachineStateConfigurer<States, Events> states) throws Exception {
        states.withStates()
              .initial(States.PLAYER_ON_TURN)
              .states(EnumSet.allOf(States.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<States, Events> transitions) throws Exception {
        transitions.withExternal()
                   .source(States.PLAYER_ON_TURN)
                   .target(States.AI_ON_TURN)
                   .event(Events.PLAYER_MAKEING_TURN)
                   .and()
                   .withExternal()
                   .source(States.AI_ON_TURN)
                   .target(States.PLAYER_ON_TURN)
                   .event(Events.AI_MAKEING_TURN);
    }
}
