package de.kaipho.schiffeversenken.statetest.statemachine;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public enum States {
    PLAYER_ON_TURN, AI_ON_TURN;
}
