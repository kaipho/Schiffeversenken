package de.kaipho.schiffeversenken.components.ai;

import de.kaipho.schiffeversenken.model.Field;

/**
 * Interface um einen Bot Gegner zu beschreiben.
 *
 * @author Tom Schmidt
 * @version 1.0
 */
public interface AI {

    /**
     * Die KI führt ihren Zug aus...
     */
    void onTurn();

    /**
     * Setzt ein Spielfeld für die KI.
     */
    void setField(Field field);

    /**
     * Liefert das Spielfeld der KI.
     *
     * @return
     */
    Field getField();
}
