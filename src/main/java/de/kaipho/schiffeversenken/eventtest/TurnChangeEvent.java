package de.kaipho.schiffeversenken.eventtest;

import de.kaipho.schiffeversenken.model.Field;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public class TurnChangeEvent {

    private final Field field;

    public TurnChangeEvent(Field field) {
        this.field = field;
    }

    public Field getField() {
        return field;
    }
}
