package de.kaipho.schiffeversenken.eventtest;

import de.kaipho.schiffeversenken.model.Field;
import de.kaipho.schiffeversenken.model.FieldSV;
import de.kaipho.schiffeversenken.service.GameSeviceImplSV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Observer...
 *
 * @author Tom Schmidt
 * @version 1.0
 */
@Component
public class AILogicBattleShip {

    @Autowired
    GameSeviceImplSV gameSevice;

    @EventListener
    public void handleTurnChange(TurnChangeEvent event) {
        System.out.println("Die KI ist am zug...");
        // ...
        int target = getShootableFields(event.getField()).get(0);
        System.out.println("Die KI schießt auf das Feld: " + target);
        FieldSV aiField = (FieldSV) event.getField();
        aiField.mark(target / 10, target % 10);
        // TODO Logik...
        System.out.println("Die KI hat ihren zug gemacht...");
    }

    private List<Integer> getShootableFields(Field field) {
        FieldSV aiField = (FieldSV) field;
        List<Integer> shootableFields = new LinkedList<>();
        for (int i = 0; i < aiField.getField().length; i++) {
            for (int j = 0; j < aiField.getField()[i].length; j++) {
                if (aiField.getField()[i][j] == null) {
                    shootableFields.add(i * 10 + j);
                }
            }
        }
        Collections.shuffle(shootableFields);
        return shootableFields;
    }
}
