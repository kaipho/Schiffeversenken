package de.kaipho.schiffeversenken.eventtest.logic;

import de.kaipho.schiffeversenken.exception.GameException;
import de.kaipho.schiffeversenken.model.Field;
import de.kaipho.schiffeversenken.model.Ship;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public class FieldActionsSV implements FieldActions<Ship> {

    @Override
    public Ship mark(Field field, int... pos) {
        if (!field.getShootable()) {
            throw new GameException("Sie sind nicht am Zug!");
        }
        return null;

/*        if (field[x][y] == LEER) {
            for (Ship ship : ships) {
                if (ship.getParts().contains(x * 10 + y)) {
                    field[x][y] = KREUZ;
                    shipParts--;
                    isShootable = false;
                    ship.destroyPart();
                    return ship;
                }
                field[x][y] = KREIS;
                isShootable = false;
            }
            return null;
        } else {
            throw new GameException("Auf dieses Feld wurde Bereits geschossen!");
        }*/
    }
}
