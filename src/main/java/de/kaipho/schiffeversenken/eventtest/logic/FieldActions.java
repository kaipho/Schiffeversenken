package de.kaipho.schiffeversenken.eventtest.logic;

import de.kaipho.schiffeversenken.model.Field;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public interface FieldActions<T> {

    T mark(Field field, int... pos);

}
