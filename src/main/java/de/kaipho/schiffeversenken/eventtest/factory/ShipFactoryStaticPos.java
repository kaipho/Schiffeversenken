package de.kaipho.schiffeversenken.eventtest.factory;

import de.kaipho.schiffeversenken.model.Ship;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
@Component
public class ShipFactoryStaticPos implements ShipFactory {

    static Ship ship21 = new Ship(2);
    {
        ship21.setParts(Arrays.asList(1 , 2));
    }
    static Ship ship22 = new Ship(2);
    {
        ship22.setParts(Arrays.asList(11 , 12));
    }
    static Ship ship23 = new Ship(2);
    {
        ship23.setParts(Arrays.asList(21 , 22));
    }
    static Ship ship24 = new Ship(2);
    {
        ship24.setParts(Arrays.asList(31 , 32));
    }
    static Ship ship31 = new Ship(3);
    {
        ship31.setParts(Arrays.asList(34 , 35, 36));
    }
    static Ship ship32 = new Ship(3);
    {
        ship32.setParts(Arrays.asList(44 , 45, 46));
    }
    static Ship ship33 = new Ship(3);
    {
        ship33.setParts(Arrays.asList(54 , 55, 56));
    }
    static Ship ship41 = new Ship(4);
    {
        ship41.setParts(Arrays.asList(60 , 61, 62, 63));
    }
    static Ship ship42 = new Ship(4);
    {
        ship42.setParts(Arrays.asList(70 , 71, 72, 73));
    }
    static Ship ship51 = new Ship(5);
    {
        ship51.setParts(Arrays.asList(80 , 81, 82, 83, 84));
    }

    @Override
    public List<Ship> getShips() {
        return Arrays.asList(ship21, ship22, ship23, ship24, ship31, ship32, ship33, ship41, ship42, ship51);
    }
}
