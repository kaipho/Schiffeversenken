package de.kaipho.schiffeversenken.eventtest.factory;

import de.kaipho.schiffeversenken.model.Ship;

import java.util.List;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public interface ShipFactory {

    List<Ship> getShips();
}
