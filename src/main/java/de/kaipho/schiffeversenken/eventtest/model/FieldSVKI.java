package de.kaipho.schiffeversenken.eventtest.model;

import de.kaipho.schiffeversenken.eventtest.factory.ShipFactory;
import de.kaipho.schiffeversenken.exception.GameException;
import de.kaipho.schiffeversenken.model.FieldSV;
import de.kaipho.schiffeversenken.model.Ship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Ein Modell f&uuml;r ein 10*10 Spielfeld von SchiffeVersenken, welches KI geeignet ist
 * Obersable...
 */
@Component(value = "AIField")
@Scope("session")
public class FieldSVKI extends FieldSV {
    public static final Boolean KREIS = Boolean.TRUE; // daneben
    public static final Boolean KREUZ = Boolean.FALSE; // getroffen
    public static final Boolean LEER = null;

    private Boolean[][] field;
    private List<Ship> ships;
    private boolean isShootable;
    private boolean isReady;

    @Autowired
    private ApplicationEventPublisher publisher;

    public boolean isReady() {
        return true;
    }

    public void setIsReady(boolean isReady) {
        this.isReady = isReady;
    }

    private int shipParts;

    @Autowired
    public FieldSVKI(ShipFactory shipFactory) {
        field = new Boolean[10][10];

        ships = shipFactory.getShips();
        shipParts = 30;
        isShootable = false;
    }

    @Override
    public void setShootable(boolean shootable) {
        this.isShootable = shootable;
        if (isWon()) {
            return;
        }

    }

    @Override
    public boolean getShootable() {
        return isShootable;
    }

    /**
     * Markiert die &uuml;bergebenen Koordinaten entweder als Wasser oder als gertoffenes Ship.
     *
     * return Das Schiff, falls eins getroffen wurde.
     *
     * @throws RuntimeException Falls bereits auf das Feld geschossen wurde.
     */
    public Ship mark(int x, int y) throws RuntimeException {
        if (!isShootable) {
            throw new GameException("Sie sind nicht am Zug!");
        }
        if (field[x][y] == LEER) {
            for (Ship ship : ships) {
                if (ship.getParts().contains(x * 10 + y)) {
                    field[x][y] = KREUZ;
                    shipParts--;
                    isShootable = false;
                    ship.destroyPart();
                    return ship;
                }
                field[x][y] = KREIS;
                isShootable = false;
            }
            return null;
        } else {
            throw new GameException("Auf dieses Feld wurde Bereits geschossen!");
        }
    }


    public List<Ship> getShips() {
        return ships;
    }

    public void setShips(List<Ship> ships) {
        this.ships = ships;
    }

    /**
     * @return true falls alle Schiffe versenkt wurden, andernfalls false.
     */
    @Override
    public boolean isWon() {
        return (shipParts == 0);
    }

    public Boolean[][] getField() {
        return field;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FieldSVKI fieldSVKI = (FieldSVKI) o;

        if (isShootable != fieldSVKI.isShootable) return false;
        if (isReady != fieldSVKI.isReady) return false;
        if (!Arrays.deepEquals(field, fieldSVKI.field)) return false;
        return !(ships != null ? !ships.equals(fieldSVKI.ships) : fieldSVKI.ships != null);

    }

    @Override
    public int hashCode() {
        int result = field != null ? Arrays.deepHashCode(field) : 0;
        result = 31 * result + (ships != null ? ships.hashCode() : 0);
        result = 31 * result + (isShootable ? 1 : 0);
        result = 31 * result + (isReady ? 1 : 0);
        return result;
    }
}
