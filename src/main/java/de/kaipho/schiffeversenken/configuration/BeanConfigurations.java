package de.kaipho.schiffeversenken.configuration;

import de.kaipho.schiffeversenken.eventtest.model.FieldSVKI;
import de.kaipho.schiffeversenken.model.FieldSV;
import de.kaipho.schiffeversenken.model.FieldTTT;
import de.kaipho.schiffeversenken.model.Player;
import de.kaipho.schiffeversenken.service.GameService;
import de.kaipho.schiffeversenken.service.LoginService;
import de.kaipho.schiffeversenken.service.LoginServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
@Configuration
public class BeanConfigurations {

    @Autowired
    GameService gameService;

    @Autowired
    ApplicationContext context;

    @Bean(name = "tttplayer")
    @Scope("prototype")
    public Player getPlayer() {
        return new Player();
    }

    @Bean(name = "tttLoginService")
    @Scope("session")
    public LoginService getLoginServiceTTT() {
        Player player = new Player();
        player.setField(new FieldTTT());
        return new LoginServiceImpl(gameService, player);
    }

    @Bean(name = "svLoginService")
    @Scope("session")
    public LoginService getLoginServiceSV() {
        Player player = new Player();
        player.setField(context.getBean("PlayerField", FieldSV.class));
        return new LoginServiceImpl(gameService, player);
    }

    @Bean(name = "ai")
    @Scope("prototype")
    public Player getAI(FieldSVKI field) {
        Player player = new Player();
        player.setName("KI");
        player.setField(field);
        return player;
    }
}
