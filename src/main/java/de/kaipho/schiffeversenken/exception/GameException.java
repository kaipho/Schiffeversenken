package de.kaipho.schiffeversenken.exception;

/**
 * Eine Exception für falsche Eingaben des Users.
 *
 * @author Tom Schmidt
 * @version 1.0
 */
public class GameException extends RuntimeException {

    public GameException(String message) {
        super(message);
    }
}
