package de.kaipho.schiffeversenken.exception;

/**
 * @author Tom Schmidt
 * @version 1.0
 */
public class GameExceptionInfo {

    private String message;
    private String type;

    public GameExceptionInfo(String message, String type) {
        this.message = message;
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
