function changeShipSelection(liElement) {
    if (liElement.firstElementChild.innerHTML == 0) {
        return false;
    }
    shipSelection.classList.remove("active");
    document.getElementById(shipSelection.id.substring(0, shipSelection.id.length - 1)).classList.remove("active");
    shipSelection = document.getElementById(liElement.id);
    shipSelection.classList.add("active");
    document.getElementById(shipSelection.id.substring(0, shipSelection.id.length - 1)).classList.add("active");
    liElement.classList.remove("over")
}
function changeTurnSelection(liElement) {
    turnSelection.classList.remove("active");
    document.getElementById(turnSelection.id.substring(0, turnSelection.id.length - 1)).classList.remove("active");
    turnSelection = document.getElementById(liElement.id);
    turnSelection.classList.add("active");
    document.getElementById(turnSelection.id.substring(0, turnSelection.id.length - 1)).classList.add("active");
    liElement.classList.remove("over")
}

function changeTurnSelectionMenu() {
    if (turnSelection.id == "horizontall") {
        changeTurnSelection(document.getElementById("vertikall"));
    } else {
        changeTurnSelection(document.getElementById("horizontall"));
    }
}

function showShipPos(field) { // field-9-0
    var size = getSelectetShipSize();
    if (turnSelection.id == "horizontall") {
        var newFieldID = field.id.substring(0, field.id.length - 1);
        var oldPos = parseInt(field.id.substring(field.id.length - 1));
        if (oldPos + size > 10) {
            document.getElementById(newFieldID + oldPos).classList.add("error");
            return;
        }
        for (var i = 0; i < size; i++) {
            document.getElementById(newFieldID + (i + oldPos)).classList.add("mark");
        }
    }
    if (turnSelection.id == "vertikall") {
        var newFieldID1 = field.id.substring(0, field.id.length - 3);
        var newFieldID2 = field.id.substring(field.id.length - 2);
        var oldPos = parseInt(field.id.substring(field.id.length - 3, field.id.length - 2));
        if (oldPos + size > 10) {
            document.getElementById(newFieldID1 + oldPos + newFieldID2).classList.add("error");
            return;
        }
        for (var i = 0; i < size; i++) {
            document.getElementById(newFieldID1 + (i + oldPos) + newFieldID2).classList.add("mark");
        }
    }
}

function markOut(liElement) {
    if (liElement.classList.contains("active")) {
        liElement.classList.remove("overBlue");
    } else {
        liElement.classList.remove("over")
    }
}

function markOver(liElement) {
    if (liElement.classList.contains("active")) {
        liElement.classList.add("overBlue");
    } else {
        liElement.classList.add("over")
    }
}

function getSelectetShipSize() {
    if (shipSelection.id == "ubootl") {
        return 2;
    } else if (shipSelection.id == "zerstoererl") {
        return 3;
    } else if (shipSelection.id == "kreuzerl") {
        return 4;
    } else {
        return 5;
    }
}

function markNextShip() {
    if (!changeShipSelection(document.getElementById("flugzeugtraegerl"))) {
        if (!changeShipSelection(document.getElementById("kreuzerl"))) {
            if (!changeShipSelection(document.getElementById("zerstoererl"))) {
                if (!changeShipSelection(document.getElementById("ubootl"))) {
                    return;
                }
            }
        }
    }
}

function endShowShipPos(field) {
    var size = getSelectetShipSize();
    if (turnSelection.id == "horizontall") {
        var newFieldID = field.id.substring(0, field.id.length - 1);
        var oldPos = parseInt(field.id.substring(field.id.length - 1));
        if (oldPos + size > 10) {
            document.getElementById(newFieldID + oldPos).classList.remove("error");
            return;
        }
        for (var i = 0; i < size; i++) {
            document.getElementById(newFieldID + (i + oldPos)).classList.remove("mark");
        }
    }
    if (turnSelection.id == "vertikall") {
        var newFieldID1 = field.id.substring(0, field.id.length - 3);
        var newFieldID2 = field.id.substring(field.id.length - 2);
        var oldPos = parseInt(field.id.substring(field.id.length - 3, field.id.length - 2));
        if (oldPos + size > 10) {
            document.getElementById(newFieldID1 + oldPos + newFieldID2).classList.remove("error");
            return;
        }
        for (var i = 0; i < size; i++) {
            document.getElementById(newFieldID1 + (i + oldPos) + newFieldID2).classList.remove("mark");
        }
    }
}

function setShip(field) {
    var size = getSelectetShipSize();
    var posy = field.id.substring(field.id.length - 3, field.id.length - 2);
    var posx = field.id.substring(field.id.length - 1);
    var horizontal = (turnSelection.id == "horizontall");
    var json = {"posx": posx, "posy": posy, "size": size, "horizontal": horizontal};

    $.ajax({
        type: 'POST',
        url: window.location.href + "/set",
        data: JSON.stringify(json),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        error: function (jqXHR) {
            onError(jqXHR, field);
        }
    });
    return false;
}

function shoot(field) {
    var size = 0;
    var posy = field.id.substring(field.id.length - 3, field.id.length - 2);
    var posx = field.id.substring(field.id.length - 1);
    var horizontal = true;
    var json = {"posx": posx, "posy": posy, "size": size, "horizontal": horizontal};

    document.getElementById("exception").style.display = "none";

    $.ajax({
        type: 'POST',
        url: window.location.href + "/shoot",
        data: JSON.stringify(json),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        error: function (jqXHR) {
            onError(jqXHR, field);
        }
    });
    return false;
}

function onError(jqXHR, field) {
    if (jqXHR.responseText == "OK") {
        shipSelection.firstElementChild.innerHTML = (shipSelection.firstElementChild.innerHTML - 1) + "";
        var size = getSelectetShipSize();
        if (turnSelection.id == "horizontall") {
            var newFieldID = field.id.substring(0, field.id.length - 1);
            var oldPos = parseInt(field.id.substring(field.id.length - 1));
            for (var i = 0; i < size; i++) {
                document.getElementById(newFieldID + (i + oldPos)).firstElementChild.src = "/images/kreuz.png";
            }
        }
        if (turnSelection.id == "vertikall") {
            var newFieldID1 = field.id.substring(0, field.id.length - 3);
            var newFieldID2 = field.id.substring(field.id.length - 2);
            var oldPos = parseInt(field.id.substring(field.id.length - 3, field.id.length - 2));
            for (var i = 0; i < size; i++) {
                document.getElementById(newFieldID1 + (i + oldPos) + newFieldID2).firstElementChild.src = "/images/kreuz.png";
            }
        }

        if (shipSelection.firstElementChild.innerHTML == 0) {
            shipSelection.lastElementChild.classList.remove("active");
            shipSelection.classList.remove("active");
            shipSelection.lastElementChild.classList.remove("fontBlue");
            shipSelection.lastElementChild.classList.add("fontGrey");
            markNextShip();
        }

    } else if (jqXHR.responseText == "RELOAD") {
        location.reload();
    }
    else if (jqXHR.responseText == "NOTHING") {

    }
    else {
        document.getElementById("exception").innerHTML = jqXHR.responseText;
        document.getElementById("exception").style.display = "block";
    }
}

function updateShip(id, newcount) {
    if (document.getElementById(id).firstElementChild.innerHTML - 1 == newcount) {
        document.getElementById(id).classList.add("error");
        setTimeout(function () {
            document.getElementById(id).classList.remove("error");
        }, 3000);
    }
    document.getElementById(id).firstElementChild.innerHTML = newcount;
}

function update() {
    $.ajax({
        type: 'POST',
        url: window.location.href + "/update",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function (notificationModelSV) {
            for (var i = 0; i < 10; i++) {
                for (var j = 0; j < 10; j++) {
                    document.getElementById("field-" + i + "-" + j).firstElementChild.src = notificationModelSV.fieldPlayer[i][j];
                }
            }
            for (var i = 0; i < 10; i++) {
                for (var j = 0; j < 10; j++) {
                    document.getElementById("fieldE-" + i + "-" + j).firstElementChild.src = notificationModelSV.fieldEnemy[i][j];
                }
            }
            if (notificationModelSV.winner != null) {
                if (notificationModelSV.winner == "even") {
                    document.getElementById("wait").innerHTML = "Unentschieden!";
                } else {
                    document.getElementById("wait").innerHTML = notificationModelSV.winner + " hat gewonnen!";
                }
                document.getElementById("wait").style.display = "block";
            } else if (notificationModelSV.onTurn) {
                document.getElementById("wait").innerHTML = "Du bis am Zug";
            } else {
                document.getElementById("wait").innerHTML = "Dein Gegner ist am Zug";
            }
            if (notificationModelSV.username == "Warten auf zweiten Spieler...") {
                document.getElementById("wait").innerHTML = notificationModelSV.username;
            }
            updateShip("flugzeugtraegers", notificationModelSV.count5P);
            updateShip("kreuzers", notificationModelSV.count4P);
            updateShip("zerstoerers", notificationModelSV.count3P);
            updateShip("uboots", notificationModelSV.count2P);

            updateShip("flugzeugtraegerg", notificationModelSV.count5E);
            updateShip("kreuzerg", notificationModelSV.count4E);
            updateShip("zerstoererg", notificationModelSV.count3E);
            updateShip("ubootg", notificationModelSV.count2E);
        }
    });
}

